// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
#include "DragonController.generated.h"

/**
*
*/
UCLASS()
class DRAGONENDEAVORS_API ADragonController : public APlayerController
{
	GENERATED_BODY()
public:
	ADragonController();

	void MoveForward(float value);
	void TurnRight(float value);

	void Flap();
	void Glide(float value);
protected:
	// Begin PlayerController interface
	//virtual void PlayerTick(float DeltaTime) override;
	virtual void SetupInputComponent() override;
	// End PlayerController interface

};
