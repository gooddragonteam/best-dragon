// Fill out your copyright notice in the Description page of Project Settings.

#include "DragonEndeavors.h"
#include "DragonController.h"
#include "Dragon.h"

ADragonController::ADragonController() {

}

void ADragonController::SetupInputComponent() {
	Super::SetupInputComponent();

	InputComponent->BindAxis("MoveForward", this, &ADragonController::MoveForward);
	InputComponent->BindAxis("TurnRight", this, &ADragonController::TurnRight);
	InputComponent->BindAxis("Glide", this, &ADragonController::Glide);
	InputComponent->BindAction("Flap", IE_Pressed, this, &ADragonController::Flap);
}


void ADragonController::MoveForward(float value) {
	if (value != 0.0f)
	{
		APawn* pawn = GetPawn();
		if (pawn != nullptr)
		{
			pawn->AddMovementInput(GetActorForwardVector(), value);
		}
	}
}
void ADragonController::TurnRight(float value) {
	if (value != 0.0f)
	{
		APawn* pawn = GetPawn();
		if (pawn != nullptr)
		{
			pawn->AddControllerYawInput(value);
		}
	}
}
void ADragonController::Flap() {
	APawn* pawn = GetPawn();
	if (pawn != nullptr)
	{
		//pawn->GetMovementComponent()->AddInputVector(GetActorUpVector()*10000000.f, true);
		pawn->LaunchPawn(GetActorUpVector()*400.f,false,true); 
	}
}
void ADragonController::Glide(float value) {
	APawn* pawn = GetPawn();
	if (pawn != nullptr && value > 0.000001f)
	{
		float fallSpeed = pawn->GetMovementComponent()->Velocity.Z;
		if (fallSpeed < 0.f) {
			pawn->GetMovementComponent()->Velocity.Z = -.1f;
		}
	}
}

