// Fill out your copyright notice in the Description page of Project Settings.

#include "DragonEndeavors.h"
#include "Dragon.h"


// Sets default values
ADragon::ADragon()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(180.0f, 55.0f);
	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);


	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->bAbsoluteRotation = false; // Want arm to rotate when character does
	CameraBoom->TargetArmLength = 160.f;
	CameraBoom->RelativeRotation = FRotator(0.f, 0.f, 0.f);

	// Create a camera...
	FollowCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	FollowCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
}

// Called when the game starts or when spawned
void ADragon::BeginPlay()
{
	Super::BeginPlay();

}




