// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameMode.h"
#include "DragonEndeavorsGameMode.generated.h"

/**
 * 
 */
UCLASS()
class DRAGONENDEAVORS_API ADragonEndeavorsGameMode : public AGameMode
{
	GENERATED_BODY()
		ADragonEndeavorsGameMode();
	
	
	
};
